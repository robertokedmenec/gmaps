<?php 
$newArray = array(
    "type" => "FeatureCollection",
    "features" => array(
        [
            "type" => "Feature",
            "properties" => array(
                "name" => "Test1",
                "Description" => "The infobox of the test1"
                
            ),
            "geometry" => array(
                "type" => "Point",
                "coordinates" => [15.981919,45.815011]
            
            )
        ],
        [
            "type" => "Feature",
            "properties" => array(
                "name" => "Test2",
                "Description" => "The infobox of the test2"
            ),
            "geometry" => array(
                "type" => "Point",
                "coordinates" => [16.981919,
                45.815011]
            
            )
        ],
        [
            "type" => "Feature",
            "properties" => array(
                "name" => "Test1",
                "Description" => "The second infobox of the test1"
            ),
            "geometry" => array(
                "type" => "Point",
                "coordinates" => [17.981919,
                45.815011]
            
            )
        ],
        [
            "type" => "Feature",
            "properties" => array(
                "name" => "Test4",
                "Description" => "The infobox of the test4"
            ),
            "geometry" => array(
                "type" => "Point",
                "coordinates" => [15.981919,
                46.815011]
            
            )
        ]
    )
);
$search_mark = isset($_GET['searched']) ? $_GET['searched'] : '';
if($search_mark != ''){
    $foundArray = array();
    foreach($newArray["features"] as $feature)
    {   
        $name = $feature["properties"]["name"];
        if(isset($name)){
            if($name == $search_mark){
                $foundArray[] = $feature;
            }   
        }
    } 
    $foundArray = array(
        "type" => "FeatureCollection",
        "features" => $foundArray
    );
    $newArray = $foundArray;
}

print json_encode($newArray);
?>