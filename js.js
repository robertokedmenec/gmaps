var map;
var bounds;
var infoWindow;

function clearMark() {
  map.data.forEach(function (feature) {
    map.data.remove(feature);

  });
}

function fitBounds() {
  bounds = new google.maps.LatLngBounds();
  map.data.forEach(function (feature) {
    feature.getGeometry().forEachLatLng(function (latlng) {
      bounds.extend(latlng);
    });
  });
  map.fitBounds(bounds);

}

function initMap() {

  map = new google.maps.Map(document.getElementById('map'), {

    maxZoom: 13,
    center: {
      lat: 45.815399,
      lng: 15.966568
    },
  });
  map.data.addGeoJson(JSON.parse(geoJson));
  fitBounds();

  var search_mark = $('input[name="search_mark"]');
  $(document).ready(function (e) {
    $('#submit').submit(function () {
      var search_mark = $('input[name=search_mark]');
      var data = {
        searched: search_mark.val()
      }
      $.ajax({
        url: "submit.php",
        type: "GET",
        data: data,
        success: function (response) {
          response = JSON.parse(response);
          if (response.features != "") {
            clearMark();
            map.data.addGeoJson(response);
            fitBounds();
          } else {
            alert("Your input was not found");
          }
        }
      });
      return false;
    });
  });

  var input = document.getElementById('pac-input');
  var searchBox = new google.maps.places.SearchBox(input);
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

  // Bias the SearchBox results towards current map's viewport.
  map.addListener('bounds_changed', function() {
    searchBox.setBounds(map.getBounds());
  });

  var markers = [];
  // Listen for the event fired when the user selects a prediction and retrieve
  // more details for that place.
  searchBox.addListener('places_changed', function() {
    var places = searchBox.getPlaces();

    if (places.length == 0) {
      return;
    }

    // Clear out the old markers.
    markers.forEach(function(marker) {
      marker.setMap(null);
    });
    markers = [];

    // For each place, get the icon, name and location.
    var bounds = new google.maps.LatLngBounds();
    places.forEach(function(place) {
      if (!place.geometry) {
        console.log("Returned place contains no geometry");
        return;
      }
      var icon = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };

      // Create a marker for each place.
      markers.push(new google.maps.Marker({
        map: map,
        icon: icon,
        title: place.name,
        position: place.geometry.location
      }));

      if (place.geometry.viewport) {
        // Only geocodes have viewport.
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
    });
    map.fitBounds(bounds);
  });
  
  infoWindow = new google.maps.InfoWindow();
  // When the user clicks, open an infowindow
  map.data.addListener('click', function(event) {
      var myHTML = event.feature.getProperty("Description");
      infoWindow.setContent(myHTML);
      infoWindow.setPosition(event.feature.getGeometry().get());
      infoWindow.setOptions({pixelOffset: new google.maps.Size(0,-30)});
      infoWindow.open(map);
  });
 
}