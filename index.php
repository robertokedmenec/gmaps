<!DOCTYPE html>
<html>

<head>
  <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
  <meta charset="utf-8">
  <title>Searchbox</title>
  <link rel="stylesheet" href="css.css">

</head>

<body>

  <form id=submit action="">
    <input id="search_mark" name="search_mark" type="text" placeholder="Search markers.." />
  </form>
  <input id="pac-input" class="controls" type="text" placeholder="Search google maps.." class="hidden" type="hidden">
  <div id="map"></div>
  <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
    crossorigin="anonymous"></script>
    <script>
      var geoJson ='<?php 
        include('submit.php');
      ?>';
    </script>
  <script src="js.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBPxVAsPU4FKD1SNZILE1wrVI7Tf0isSFw&libraries=places&callback=initMap"
    async defer></script>
</body>

</html>