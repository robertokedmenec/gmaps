eqfeed_callback({

        "type": "FeatureCollection",
        "features": [
          {
            "type": "Feature",
            "properties": {
              name:"Test1"
            },
            "geometry": {
              "type": "Point",
              "coordinates": [
                15.981919,
                45.815011
              ]
            }
          },
          {
            "type": "Feature",
            "properties": {
              name:"Test2"
            },
            "geometry": {
              "type": "Point",
              "coordinates": [
                16.981919,
                45.815011
              ]
            }
          },
          {
            "type": "Feature",
            "properties": {
              name:"Test3"
            },
            "geometry": {
              "type": "Point",
              "coordinates": [
                17.981919,
                45.815011
              ]
            }
          },
          {
            "type": "Feature",
            "properties": {
              name:"Test4"
            },
            "geometry": {
              "type": "Point",
              "coordinates": [
                15.981919,
                46.815011
              ]
            }
          },
          {
            "type": "Feature",
            "properties": {
              name:"Test5"
            },
            "geometry": {
              "type": "Point",
              "coordinates": [
                15.979614257812498,
                45.80965764997408
              ]
            }
          }
          ,
          {
            "type": "Feature",
            "properties": {
              name:"Test6"
            },
            "geometry": {
              "type": "Point",
              "coordinates": [
                16.4379653,
                46.3897383
              ]
            }
          }
        ] 
});